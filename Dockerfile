FROM nginx:latest
COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN mkdir /arch
COPY frontend-0.0.1-SNAPSHOT.tar.gz /arch/frontend-0.0.1-SNAPSHOT.tar.gz

RUN tar -xvpzf /arch/frontend-0.0.1-SNAPSHOT.tar.gz -C /usr/share/nginx/html